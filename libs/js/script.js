$(window).on('load', function() {
    if ($('#preloader').length) {
      $('#preloader').delay(100).fadeOut('slow', function() {
        $(this).remove();
      });
    }
    AOS.init();
});

$(document).scroll(() => {
    let scrollPos = $(this).scrollTop();
    if (scrollPos < 30) {
        $('#header').css('background-color', 'transparent');
    } else {
        $('#header').css('background-color', '#2A385D');
    }
});

$('#scroll-down').click(() => {
    let headerHeight = parseInt($('#header').css('height'), 10);
    $([document.documentElement, document.body]).animate({
        scrollTop: $('#services').offset().top - headerHeight
    }, 1500);
});

$('#clients-carousel').owlCarousel({
    loop:true,
    margin:60,
    center:true,
    nav:true,
    autoplay: true,
    autoplayTimeout: 3000,
    fluidSpeed: true,
    autoplaySpeed: 1000,
    autoplayHoverPause: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
})
$('#testimonial-carousel').owlCarousel({
    loop:true,
    margin:60,
    center:true,
    nav:false,
    dots: false,
    mouseDrag: false,
    touchDrag: false,
    responsive:{
        0:{
            items:1
        }
    }
})

var owl = $('#testimonial-carousel');
// Go to the next item
$('#test-next').click(function() {
    owl.trigger('next.owl.carousel', [500]);
})
// Go to the previous item
$('#test-prev').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owl.trigger('prev.owl.carousel', [500]);
})

$('#menu').click(() => {
    $('#sidebar').css('transform', 'translateX(0)');
});
    
$('#side-bar-x').click(() => {
    let x = window.matchMedia("(max-width: 450px)");
    if (x.matches) {
        $('#sidebar').css('transform', 'translateX(300px)');
    } else {
        $('#sidebar').css('transform', 'translateX(420px)');
    }
})

function hideSideBar() {
    let x = window.matchMedia("(max-width: 450px)");
    if (x.matches) {
        $('#sidebar').css('transform', 'translateX(300px)');
    } else {
        $('#sidebar').css('transform', 'translateX(420px)');
    }
}
window.onresize = hideSideBar;









$('.blue-btn').mousedown(event => {
    $(event.target).closest('button').css('background-color', '#2A385D');
})
$('.blue-btn').mouseup(event => {
    $(event.target).closest('button').css('background-color', '#0082BE');
})
$('.blue-btn').blur(event => {
    $(event.target).closest('button').css('background-color', '#0082BE');
})

$('.service').mouseover(event => {
    let img = $(event.target).closest('.service').find('img');
    let box = $(event.target).closest('.service').find('.services-info');
    img.css('transform', 'scale(1.1)');
    box.css('background-color', 'rgba(140,195,231,0.4)');
})
$('.service').mouseout(event => {
    let img = $(event.target).closest('.service').find('img');
    let box = $(event.target).closest('.service').find('.services-info');
    img.css('transform', 'scale(1)');
    box.css('background-color', '#D6D8D7');
})




$('#m-global').mouseover(event => {
    let img = $(event.target).closest('#m-global').find('img');
    let box = $(event.target).closest('#m-global').find('#m-global-info');
    img.css('transform', 'scale(1.1)');
    box.css('background-color', 'rgba(140,195,231,0.4)');
})
$('#m-global').mouseout(event => {
    let img = $(event.target).closest('#m-global').find('img');
    let box = $(event.target).closest('#m-global').find('#m-global-info');
    img.css('transform', 'scale(1)');
    box.css('background-color', '#D6D8D7');
})




$('.post').mouseover(event => {
    let img = $(event.target).closest('.post').find('img');
    let box = $(event.target).closest('.post');
    img.css('transform', 'scale(1.05)');
    box.css('background-color', 'rgba(140,195,231,0.4)');
})
$('.post').mouseout(event => {
    let img = $(event.target).closest('.post').find('img');
    let box = $(event.target).closest('.post');
    img.css('transform', 'scale(1)');
    box.css('background-color', '#D6D8D7');
})

















